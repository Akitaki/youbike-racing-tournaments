{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    zsh cmake gnumake gdb clang-tools clang_11
  ];
}
