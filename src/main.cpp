#include <algorithm>
#include <array>
#include <iostream>
#include <stack>
#include <unordered_set>
#include <vector>

void testcase();

int main() {
    std::cin.tie(nullptr);
    std::ios_base::sync_with_stdio(false);

    int testcases;
    std::cin >> testcases;

    while (testcases--) {
        testcase();
    }

    return 0;
}

int flood(int from, int current,
          std::vector<std::vector<std::pair<int, int>>>& adj,
          std::vector<bool>& specials) {

    int value = specials[current] ? 1 : 0;

    for (auto& p : adj[current]) {
        int neighbor = p.first;
        if (neighbor == from) {
            continue;
        }

        if (p.second != -1) {
            value += p.second;
            continue;
        }

        int spnodes_below = flood(current, neighbor, adj, specials);
        p.second = spnodes_below;
        value += spnodes_below;
    }

    return value;
}

void flood2(int from, int current, std::vector<int>& nodes,
            std::vector<std::vector<std::pair<int, int>>>& adj,
            std::vector<bool>& specials) {
    if (specials[current]) {
        nodes.push_back(current);
    }

    for (auto p : adj[current]) {
        int neighbor = p.first;
        if (neighbor == from) {
            continue;
        }
        if (p.second == 0) {
            continue;
        }
        flood2(current, neighbor, nodes, adj, specials);
    }
}

void testcase() {
    int n, m;
    std::cin >> n >> m;

    std::vector<std::vector<std::pair<int, int>>> adj(n);
    std::vector<bool> specials(n);

    for (int i = 0; i < n - 1; i += 1) {
        int a, b;
        std::cin >> a >> b;
        adj[a].emplace_back(b, -1);
        adj[b].emplace_back(a, -1);
    }

    for (int i = 0; i < 2 * m; i += 1) {
        int node;
        std::cin >> node;
        specials[node] = true;
    }

    int candidate = 0;
    std::vector<std::pair<int, int>> edges;
    while (true) {
        flood(-1, candidate, adj, specials);

        edges = adj[candidate];
        edges.erase(std::remove_if(edges.begin(), edges.end(),
                                   [](std::pair<int, int> edge) {
                                       return edge.second == 0;
                                   }),
                    edges.end());

        const bool candidate_is_special = specials[candidate];
        if (candidate_is_special) {
            edges.emplace_back(candidate, 1);
        }

        int sum = 0;
        for (auto p : edges) {
            sum += p.second;
        }

        const bool is_legal = std::none_of(
            edges.begin(), edges.end(),
            [=](std::pair<int, int> edge) { return edge.second > sum / 2; });

        if (is_legal) {
            break;
        } else {
            candidate = std::max_element(edges.begin(), edges.end(),
                                         [](std::pair<int, int> e_a,
                                            std::pair<int, int> e_b) {
                                             return e_a.second < e_b.second;
                                         })
                            ->first;
        }
    }

    // Pair nodes
    const int center = candidate;
    std::vector<int> nodes;
    std::sort(edges.begin(), edges.end(),
              [](std::pair<int, int> e_a, std::pair<int, int> e_b) {
                  return e_a.second > e_b.second;
              });
    for (auto p : edges) {
        if (p.first == center) {
            nodes.push_back(center);
        } else {
            flood2(center, p.first, nodes, adj, specials);
        }
    }
    std::vector<int> first;
    std::vector<int> second;
    bool f = true;
    for (int node : nodes) {
        if (f) {
            first.push_back(node);
        } else {
            second.push_back(node);
        }
        f = !f;
    }
    std::rotate(second.begin(), second.begin() + second.size() / 2,
                second.end());

    // Output
    std::cout << "1\n";
    std::cout << center << "\n";

    for (int i = 0; i < first.size(); i += 1) {
        int a = first[i];
        int b = second[i];
        std::cout << a << " " << b << " " << center << "\n";
    }
}
